#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Usage of Albumentations

"""
import sys 
import os
import math
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
import segmentation_models as sm
import albumentations as A
import matplotlib.pyplot as plt
import pickle
from datetime import datetime
sys.path.append("/home/kent/college/Thesis/myThesisCode/")
from Create_models_v2 import load_model, load_original_dataset, load_original_classes, plot_history_from_folder
from Create_models_v2 import SatelliteAugmentationGenerator, SatellitePatchesGenerator, get_preprocessing, reverse_one_hot
from Create_models_v2 import plot_logger_from_folder, SAVE_PATH, EPOCHS, SAMPLES, STOPPING_PATIENCE, split_dataset, get_augmentation_for_train
from Create_models_v2 import DATASET_DIR, Patches, one_hot_encode
sm.set_framework('tf.keras')
sm.framework()

def visualize(**images):
    """PLot images in one row."""
    n = len(images)
    plt.figure(figsize=(10, 5))
    for i, (name, image) in enumerate(images.items()):
        plt.subplot(1, n, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.title(' '.join(name.split('_')).title() +"\n" +str(image.shape))
        plt.imshow(image.astype("uint8"))
    plt.show()
    
def show_augmentations():
    df = load_original_dataset()
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    
    # sample = df.sample(1) #'image id 513968'
    sample = df.query('image_id == 513968')
    
    original_image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
        sample["sat_image_path"].values[0]), dtype="uint8")
 
    mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
        sample["mask_path"].values[0]), dtype="uint8")
        
    visualize(
        original_image=original_image,
        mask=mask
    )
    
    transform = A.Compose([
        A.RandomCrop(height=512, width=512, always_apply=True),
    ])
    image = transform(image=original_image)['image']
    
    transform = A.Compose([
        A.RandomBrightnessContrast(brightness_limit=0.3, contrast_limit=0.3, p=1.0),
    ])
    RandomBrightnessContrast = transform(image=image)['image']
    transform = A.Compose([
        A.VerticalFlip(p=1.0),
    ])
    VerticalFlip = transform(image=image)['image']
    transform = A.Compose([
        A.RandomRotate90(p=1.0),
    ])
    RandomRotate90 = transform(image=image)['image']
    visualize(
        cropped_image=image,
        RandomBrightnessContrast=RandomBrightnessContrast,
        VerticalFlip=VerticalFlip,
        RandomRotate90=RandomRotate90,
    )
    
def show_hot_encoding():
    df = load_original_dataset()
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    
    # sample = df.sample(1) #'image id 513968'
    sample = df.query('image_id == 513968')
    original_image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
        sample["sat_image_path"].values[0]), dtype="uint8")
 
   
    original_mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
        sample["mask_path"].values[0]), dtype="uint8")

    transform = A.Compose([
        A.RandomCrop(height=512, width=512, always_apply=True),
    ])
    transformed_sample = transform(image=original_image,mask=original_mask)
    image  = transformed_sample["image"]
    mask  = transformed_sample["mask"]
    mask_oh = one_hot_encode(mask, satellite_class_rgb_values).astype('float')
    
    # plt.figure(figsize=(13, 13))
    plt.figure()
    num_classes = len(satellite_class_list)
    # plt.subplot(4, 4, 1)
    plt.xticks([])
    plt.yticks([])
    plt.title("RGB Mask")
    plt.imshow(mask)
    plt.show()
    for i in range(num_classes):
        # plt.subplot(4, 4, i+2)
        plt.figure()
        plt.xticks([])
        plt.yticks([])
        plt.title(satellite_class_list[i])
        plt.imshow(mask_oh[:,:,i], cmap="binary")
        plt.show()

    
    import matplotlib.patches as mpatches
    import matplotlib.pyplot as plt
    legs = []
    for i in range(num_classes):
        legs.append(mpatches.Patch(color=satellite_class_rgb_values[i]/255, label=satellite_class_list[i]))
    plt.legend(handles=legs, loc=2, prop={'size': 16})
    
    plt.show()