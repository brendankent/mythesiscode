# -*- coding: utf-8 -*-
# from google.colab import drive
# drive.mount('/content/drive')

# !pip install --upgrade pip
# !pip install opencv-python-headless==4.1.2.30
# !pip install -U albumentations
# !pip install segmentation-models
# !mkdir /home/deepglobe/
# !cp -r /content/drive/MyDrive/deepglobe/* /home/deepglobe/
# !ls /home/deepglobe/train/ | wc -l 
"""
CNN: Unet
"""
                                    
import os
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
import segmentation_models as sm
import albumentations as A
import matplotlib.pyplot as plt
import pickle
from tqdm import tqdm
from datetime import datetime
sm.set_framework('tf.keras')
sm.framework()
print(tf.keras.__version__)

DATASET_DIR = "/home/kent/college/Thesis/deepglobe/"
SAVE_PATH = "/home/kent/college/Thesis/save/"
TEST_PATCHES_DIR = "/home/kent/college/Thesis/test_patches/"
EPOCHS=50
SAMPLES=0 # used to speed up debugging, should be zero for  
STOPPING_PATIENCE=15

class Patches(tf.keras.layers.Layer):
    def __init__(self, patch_size):
        super(Patches, self).__init__()
        self.patch_size = patch_size

    def call(self, images):
        batch_size = tf.shape(images)[0]
        patches = tf.image.extract_patches(
            images=images,
            sizes=[1, self.patch_size, self.patch_size, 1],
            strides=[1, self.patch_size, self.patch_size, 1],
            rates=[1, 1, 1, 1],
            padding="VALID",
        )
        patch_dims = patches.shape[-1]
        patches = tf.reshape(patches, [batch_size, -1, patch_dims])
        return patches
    
def make_image_cuts(image_id, image_path, target_path, save_dir):
    patch_size = 512  # Size of the patches to be extract from the input images
    image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(image_path))
    mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(target_path))
    images = tf.convert_to_tensor([image,mask])
    patches = Patches(patch_size)(images)
    image_patches_list = []
    mask_patches_list = []
    for k in [0,1]:
        for i, patch in enumerate(patches[k]): # images
            patch_img = tf.reshape(patch, (patch_size, patch_size, 3))
            if k:
                save_name = "{}_{:02d}_mask.png".format(image_id, i)
                mask_patches_list.append(os.path.join(save_dir, save_name))
            else:
                save_name = "{}_{:02d}_sat.png".format(image_id, i)
                image_patches_list.append(os.path.join(save_dir, save_name))
            tf.keras.utils.save_img(
                path=os.path.join(save_dir, save_name), x=patch_img.numpy().astype("uint8")
            )
    assert len(image_patches_list) == len(mask_patches_list)
    return image_patches_list, mask_patches_list

def make_test_patches():
    test_df = pd.read_csv(os.path.join(DATASET_DIR, 'put_away.csv'))
    metadata_patches = []
    with tqdm(total=test_df.shape[0]) as pbar:
        for index, row in test_df.iterrows():
            image_patches_list, mask_patches_list = make_image_cuts(image_id=row["image_id"], image_path=row["sat_image_path"],
                            target_path=row["mask_path"], save_dir=TEST_PATCHES_DIR)
            
            for num in range(len(image_patches_list)):
                entry = dict(row)
                entry["sat_image_patch"] = image_patches_list[num]
                entry["mask_patch"] = mask_patches_list[num]
                metadata_patches.append(entry)
                
            pbar.update(1)
            # if index > 10:
            #     break
            
    test_patches_df = pd.DataFrame(metadata_patches)
    test_patches_df = test_patches_df.sample(frac=1) # shuffle the set
    test_patches_df.to_csv(os.path.join(DATASET_DIR, 'put_away_patches.csv'), index=False)
    return test_patches_df
    
class SatelliteTestPatchesGenerator(tf.keras.utils.Sequence):
    """This generator is only for the test set
    it works on already patches images
    """

    def __init__(self, batch_size, 
                 satellite_class_rgb_values, data, 
                 preprocessing=None):
        self.img_size = (512, 512)
        self.batch_size = batch_size
        self.data = data
        self.preprocessing = preprocessing
        self.satellite_class_rgb_values = satellite_class_rgb_values
        self.indices = np.arange(self.data.shape[0])

    def __len__(self):
        return len(self.data) // self.batch_size

    def __getitem__(self, idx):
        """Returns tuple (input, target) correspond to batch #idx."""
        inds = self.indices[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_data = self.data.iloc[inds]
        
        batch_input_img_paths = batch_data["sat_image_patch"]
        batch_target_img_paths = batch_data["mask_patch"]
        
        x = np.zeros((self.batch_size,) +
                     self.img_size + (3,), 
                     dtype="float32")
        y = np.zeros((self.batch_size,) + 
                     self.img_size + (self.satellite_class_rgb_values.shape[0],), 
                     dtype="float32")
        for num in range(len(batch_data)):
            image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
                batch_input_img_paths.iloc[num]), dtype="uint8")
         
            mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
                batch_target_img_paths.iloc[num]), dtype="uint8")
                          
            if self.preprocessing:
                sample = self.preprocessing(image=image, mask=mask)
                image, mask = sample['image'], sample['mask']
                
            image = (image / 255.0).astype("float")
            mask = one_hot_encode(mask, 
                            self.satellite_class_rgb_values).astype('float')
            x[num] = image
            y[num] = mask
        return x, y
    
    
class SatellitePatchesGenerator(tf.keras.utils.Sequence):
    """Generator which returns all the patches from a 
        single image. 
        2448 x 2448 image 
        512 x 512 patches 
        16 paches per image """

    def __init__(self, 
                 satellite_class_rgb_values, data, 
                 preprocessing=None):
        self.img_size = (512, 512)
        self.orig_img_size = (2448, 2448)
        self.total_num_patches = int(self.orig_img_size[0] /self.img_size[0])**2
        self.data = data
        self.preprocessing = preprocessing
        self.satellite_class_rgb_values = satellite_class_rgb_values
        self.indices = np.arange(self.data.shape[0])


    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        batch_data = self.data.iloc[idx]
        
        batch_input_img_path = batch_data["sat_image_path"]
        batch_target_img_path = batch_data["mask_path"]
        
        x = np.zeros((self.total_num_patches,) +
                     self.img_size + (3,), 
                     dtype="float32")
        y = np.zeros((self.total_num_patches,) + 
                      self.img_size + (self.satellite_class_rgb_values.shape[0],), 
                      dtype="float32")
        image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            batch_input_img_path), dtype="uint8")
     
        mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            batch_target_img_path), dtype="uint8")
            
        tf_img = tf.convert_to_tensor(np.asarray([image,mask]))
        patches = Patches(self.img_size[0])(tf_img)
        for p_num in range(self.total_num_patches):
            patch_img = tf.reshape(patches[0][p_num], self.img_size + (3,)).numpy()
            patch_mask = tf.reshape(patches[1][p_num], self.img_size + (3,)).numpy()
                
            if self.preprocessing:
                sample = self.preprocessing(image=patch_img, mask=patch_mask)
                patch_img, patch_mask = sample['image'], sample['mask']
                
            patch_img = (patch_img / 255.0).astype("float")
            patch_mask = one_hot_encode(patch_mask, 
                            self.satellite_class_rgb_values).astype('float')
            x[p_num] = patch_img
            y[p_num] = patch_mask
           
        return x, y
            

def get_augmentation_for_train():
    return A.Compose([
        A.RandomCrop(height=512, width=512, always_apply=True),
        A.VerticalFlip(p=0.5),              
        A.RandomRotate90(p=0.5),
        A.RandomBrightnessContrast(p=0.2),
        ]
    )

def get_augmentation_for_val():
    return A.Compose([
        A.CenterCrop(height=512, width=512, always_apply=True),
        ]
    )

def get_preprocessing(preprocessing_fn):
    return A.Compose([
        A.Lambda(image=preprocessing_fn),
    ])

# Perform one hot encoding on label
def one_hot_encode(label, label_values):
    """
    Convert a segmentation image label array to one-hot format
    by replacing each pixel value with a vector of length num_classes
    # Arguments
        label: The 2D array segmentation image label
        label_values
        
    # Returns
        A 2D array with the same width and hieght as the input, but
        with a depth size of num_classes
    """
    semantic_map = []
    for colour in label_values:
        equality = np.equal(label, colour)
        class_map = np.all(equality, axis = -1)
        semantic_map.append(class_map)
    semantic_map = np.stack(semantic_map, axis=-1)

    return semantic_map

# Perform reverse one-hot-encoding on labels / preds
def reverse_one_hot(image):
    """
    Transform a 2D array in one-hot format (depth is num_classes),
    to a 2D array with only 1 channel, where each pixel value is
    the classified class key.
    # Arguments
        image: The one-hot format image 
        
    # Returns
        A 2D array with the same width and hieght as the input, but
        with a depth size of 1, where each pixel value is the classified 
        class key.
    """
    x = np.argmax(image, axis = -1)
    return x


class SatelliteAugmentationGenerator(tf.keras.utils.Sequence):
    """Generator for satellite images with augmentation  
       using the augementation function
       Random 512 x 512 crop
       random VerticalFlip 
       random RandomRotate90"""

    def __init__(self, batch_size, 
                 satellite_class_rgb_values, data, 
                 augmentation=None, preprocessing=None,
                 shuffle_on_epoch_end=True):
        self.img_size = (512, 512)
        self.batch_size = batch_size
        self.data = data
        self.augmentation = augmentation
        self.preprocessing = preprocessing
        self.satellite_class_rgb_values = satellite_class_rgb_values
        self.indices = np.arange(self.data.shape[0])
        self.shuffle_on_epoch_end = shuffle_on_epoch_end


    def __len__(self):
        return len(self.data) // self.batch_size

    def __getitem__(self, idx):
        """Returns tuple (input, target) correspond to batch #idx."""
        inds = self.indices[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_data = self.data.iloc[inds]
        
        batch_input_img_paths = batch_data["sat_image_path"]
        batch_target_img_paths = batch_data["mask_path"]
        
        x = np.zeros((self.batch_size,) +
                     self.img_size + (3,), 
                     dtype="float32")
        y = np.zeros((self.batch_size,) + 
                     self.img_size + (self.satellite_class_rgb_values.shape[0],), 
                     dtype="float32")
        for num in range(len(batch_data)):
            image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
                batch_input_img_paths.iloc[num]), dtype="uint8")
         
            mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
                batch_target_img_paths.iloc[num]), dtype="uint8")
            
            if self.augmentation:
                sample = self.augmentation(image=image, mask=mask)
                image, mask = sample['image'], sample['mask']
                
            if self.preprocessing:
                sample = self.preprocessing(image=image, mask=mask)
                image, mask = sample['image'], sample['mask']
                
            image = (image / 255.0).astype("float")
            mask = one_hot_encode(mask, 
                            self.satellite_class_rgb_values).astype('float')
            x[num] = image
            y[num] = mask
            
        return x, y
    
    def on_epoch_end(self):
        if self.shuffle_on_epoch_end:
            np.random.shuffle(self.indices)
        
def split_put_away_test():
    """Used to split the test from the rest at the very start"""
    metadata_df = load_original_dataset()
    put_away_df = metadata_df.sample(frac=0.1)
    metadata_df.drop(put_away_df.index, inplace=True)
    put_away_df.to_csv(os.path.join(DATASET_DIR, "put_away.csv"),  index=False)
    
def drop_the_put_away(df):
    """Remove the put away images"""
    put_away_df = pd.read_csv(os.path.join(DATASET_DIR, 'put_away.csv'))
    return df.drop(put_away_df.index)
    
def split_dataset(df):
    """train - val  - test
        80 % - 10 % - 10%
        
    of the 723 rows, 80 in put away    
    also saved a put away which is never used in model making
    """
    df = drop_the_put_away(df)
    
    if SAMPLES:
        df = df.sample(SAMPLES) # to make testing faster
    test_split = 0.10
    
    # Initial train and test split.
    train_df, val_df = train_test_split(
        df,
        test_size=test_split,
        shuffle=True
    )
    
    # Splitting the test set further into validation
    # and new test sets.
    # val_df = test_df.sample(frac=0.5)
    # test_df.drop(val_df.index, inplace=True)
    return train_df, val_df
    
def make_model(train_df, val_df, dtime_str, satellite_class_rgb_values):
    model_name  = dtime_str
    if not os.path.exists(os.path.join(SAVE_PATH, model_name)):
        os.mkdir(os.path.join(SAVE_PATH, model_name))
    full_directory = os.path.join(SAVE_PATH, model_name)
    
    batch_size = 8
    BACKBONE = 'resnet34'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    
    train_gen = SatelliteAugmentationGenerator(
        batch_size=batch_size, 
        satellite_class_rgb_values=satellite_class_rgb_values,
        data=train_df, 
        augmentation=get_augmentation_for_train(),
        preprocessing=get_preprocessing(preprocess_input),
    )

    val_gen = SatellitePatchesGenerator(
        satellite_class_rgb_values=satellite_class_rgb_values,
        data=val_df, 
        preprocessing=get_preprocessing(preprocess_input),
    )
    
    # a = val_gen[0]
    # plt.figure(figsize=(10, 10))
    # n=4
    # for i in range(16):
    #     ax = plt.subplot(n, n, i + 1)
    #     plt.imshow(a[0][i,:,:,:])
    #     plt.axis("off")
    # plt.show()
    # plt.figure(figsize=(10, 10))
    # for i in range(16):
    #     ax = plt.subplot(n, n, i + 1)
    #     plt.imshow(reverse_one_hot(a[1][i,:,:,:]))
    #     plt.axis("off")
    # plt.show()
       
    model = sm.Unet(BACKBONE, encoder_weights='imagenet', classes=7,
                  activation="softmax", encoder_freeze=True)

    optim = tf.keras.optimizers.Adam(0.001)
    
    # Segmentation models losses can be combined together by '+' and scaled by integer or float factor
    dice_loss = sm.losses.DiceLoss()
    focal_loss = sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss) # if time allwos, play with this 1 <---
       
    # model.compile(optim, total_loss, metrics)
    model.compile(optimizer=optim,
              loss=total_loss,
              metrics=[tf.keras.metrics.CategoricalAccuracy(name="categorical_accuracy", dtype=None),
                       tf.keras.metrics.OneHotMeanIoU(name="onehot_mean_iou", num_classes=7),
                       # tf.keras.metrics.MeanIoU(name="mean_iou", num_classes=7),
                       tf.keras.metrics.OneHotIoU(name="iou_0", num_classes=7, target_class_ids=[0]),
                       tf.keras.metrics.OneHotIoU(name="iou_1",num_classes=7, target_class_ids=[1]),
                       tf.keras.metrics.OneHotIoU(name="iou_2",num_classes=7, target_class_ids=[2]),
                       tf.keras.metrics.OneHotIoU(name="iou_3",num_classes=7, target_class_ids=[3]),
                       tf.keras.metrics.OneHotIoU(name="iou_4",num_classes=7, target_class_ids=[4]),
                       tf.keras.metrics.OneHotIoU(name="iou_5",num_classes=7, target_class_ids=[5]),
                       tf.keras.metrics.OneHotIoU(name="iou_6",num_classes=7, target_class_ids=[6]),
                       sm.metrics.IOUScore(name="sm_iou_score", threshold=0.5, per_image=True)
                       # tf.keras.metrics.IoU(num_classes=2, target_class_id=[0])
                       ])


    # model.summary()
   
    callbacks = [
    tf.keras.callbacks.ModelCheckpoint(os.path.join(full_directory, "best_model.h5"), 
                        mode='max', save_best_only=True, verbose=1, monitor='val_onehot_mean_iou'),
    tf.keras.callbacks.CSVLogger(os.path.join(full_directory, "logger.csv"), append=True),
    tf.keras.callbacks.EarlyStopping(monitor="val_onehot_mean_iou", patience=STOPPING_PATIENCE, verbose=1),
    ]
    history = model.fit(
        train_gen, 
        epochs=EPOCHS, 
        callbacks=callbacks, 
        validation_data=val_gen,
        # verbose=2
    )
    
    with open(os.path.join(full_directory, "history.pickle"), 'wb') as f:
        pickle.dump(history.history, f)
    return model

    

def load_original_classes():  
    """Get all the satellite metadata, list of classes, list of indices, RGB values"""
    class_dict = pd.read_csv(os.path.join(DATASET_DIR, 'class_dict.csv'))
    # Get class names
    satellite_class_list = class_dict['name'].tolist()
    # Get class RGB values
    class_rgb_values = class_dict[['r','g','b']].values.tolist()
        
    # Get RGB values of required classes
    satellite_class_indices = [satellite_class_list.index(cls.lower()) for cls in satellite_class_list]
    satellite_class_rgb_values =  np.array(class_rgb_values)[satellite_class_indices]
    
    return satellite_class_list, satellite_class_indices, satellite_class_rgb_values


def load_original_dataset():
    """Read the orginal dataset from DeepGlobe from the disk"""
    metadata_df = pd.read_csv(os.path.join(DATASET_DIR, 'metadata.csv'))
    metadata_df = (metadata_df
                    .query('split in ["train"]')
                    )
    replace_path = lambda x: os.path.join(DATASET_DIR, x)
    metadata_df = metadata_df.copy()
    metadata_df['sat_image_path'] = metadata_df['sat_image_path'].map(replace_path)
    metadata_df['mask_path'] = metadata_df['mask_path'].map(replace_path)

    return metadata_df

        
def load_model(model_path):
    """Load the U-Net model from disk so that it can be used for predicting or
    re-training"""
    dice_loss = sm.losses.DiceLoss()
    focal_loss = sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss)
    return tf.keras.models.load_model(model_path,
            custom_objects={
                'dice_loss_plus_1focal_loss': total_loss,
                'sm_iou_score': sm.metrics.IOUScore(threshold=0.5, per_image=True),
                'onehot_mean_iou': tf.keras.metrics.OneHotMeanIoU(num_classes=7),
                'categorical_accuracy': tf.keras.metrics.CategoricalAccuracy(),
                'iou_0': tf.keras.metrics.OneHotIoU(num_classes=7, target_class_ids=[0]),
                'iou_1': tf.keras.metrics.OneHotIoU(num_classes=7, target_class_ids=[1]),
                'iou_2': tf.keras.metrics.OneHotIoU(num_classes=7, target_class_ids=[2]),
                'iou_3': tf.keras.metrics.OneHotIoU(num_classes=7, target_class_ids=[3]),
                'iou_4': tf.keras.metrics.OneHotIoU(num_classes=7, target_class_ids=[4]),
                'iou_5': tf.keras.metrics.OneHotIoU(num_classes=7, target_class_ids=[5]),
                'iou_6': tf.keras.metrics.OneHotIoU(num_classes=7, target_class_ids=[6])
            })
            
    
def start_training():
    """Most important method, start training a U-Net model"""
    metadata_df = load_original_dataset()
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()

    dtime_str = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
    
    train_df, val_df = split_dataset(metadata_df)
    print("train_df",  train_df.shape)
    print("val_df",  val_df.shape)
    
    m = make_model(train_df, val_df, dtime_str, satellite_class_rgb_values)

    return m

def start_testing(model_dir): 
    """start testing the model on whatever is the test.csv file"""
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    
    test_df = pd.read_csv(os.path.join(model_dir, 'test.csv'))
    
    BACKBONE = 'resnet34'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    test_gen = SatellitePatchesGenerator(
        satellite_class_rgb_values=satellite_class_rgb_values,
        data=test_df, 
        preprocessing=get_preprocessing(preprocess_input),
    )
    m = load_model(os.path.join(model_dir,"best_model.h5"))
    print("Generate predictions")
    p = m.predict(test_gen)
    print("predictions shape:", p.shape)
    print("Evaluate:")
    scores = m.evaluate(test_gen)
    print(scores)
    
    
def plot_history(history):
    """Given a history dict, chart the metrics"""
    plt.style.use('seaborn-whitegrid')   
    plt.rcParams['figure.dpi'] = 120
    plt.rcParams['font.family'] = ['sans-serif']
    plt.rcParams.update({'font.size': 13})
    
    plt.figure(figsize=(12, 5))
    plt.plot(history['categorical_accuracy'])
    plt.plot(history['val_categorical_accuracy'])
    plt.title('Model categorical_accuracy')
    plt.ylabel('categorical_accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='upper left')
    plt.show()
    
    plt.figure(figsize=(12, 5))
    plt.plot(history['onehot_mean_iou'])
    plt.plot(history['val_onehot_mean_iou'])
    plt.title('Model onehot_mean_iou')
    plt.ylabel('onehot_mean_iou')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='upper left')
    plt.show()
    
    plt.figure(figsize=(12, 5))
    plt.plot(history['loss'])
    plt.plot(history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='upper left')
    plt.show()
 
    plt.figure(figsize=(12, 5))
    for metric in "iou_0,iou_1,iou_2,iou_3,iou_4,iou_5,iou_6".split(","):
        plt.plot(history[metric])

    plt.title('Model Training iou per class')
    plt.ylabel('IoU')
    plt.xlabel('Epoch')
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    plt.legend(satellite_class_list,
               loc='upper left')
    plt.show()
    
    plt.figure(figsize=(12, 5))
    for metric in "val_iou_0,val_iou_1,val_iou_2,val_iou_3,val_iou_4,val_iou_5,val_iou_6".split(","):
        plt.plot(history[metric])

    plt.title('Model Validation iou per class')
    plt.ylabel('IoU')
    plt.xlabel('Epoch')
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    plt.legend(satellite_class_list,
               loc='upper left')
    plt.show()
    
def plot_history_from_folder(save_directory):
    """From the save directory, use the history.pickle file to plot the metrics"""
    with open(os.path.join(save_directory, "history.pickle"), 'rb') as f:
        history = pickle.load(f)
        plot_history(history)
            
def plot_logger_from_folder(save_directory):
    """From the save directory, use the logger.csv file to plot the metrics"""
    history = pd.read_csv(os.path.join(save_directory, 'logger.csv'))
    plot_history(history)


    
if __name__ == '__main__':

    # for i in range(20):
        # start_training()
    pass
        
    # start_testing("/home/kent/college/tmp/model_Apr_12")
    # plot_logger_from_folder("/home/kent/college/tmp/13_04_2022_20_30_12")
    # start_testing("/home/kent/college/tmp/13_04_2022_20_30_12")
