#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
2nd phase

Data preparation split into Train, Validation, Test
and train CNN model

T    - V    - T
80 % - 10 % - 10%

CNN: Unet
"""

import os
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
import numpy as np
import segmentation_models as sm
import albumentations as A


sm.set_framework('tf.keras')
sm.framework()

class Patches(tf.keras.layers.Layer):
    def __init__(self, patch_size):
        super(Patches, self).__init__()
        self.patch_size = patch_size

    def call(self, images):
        batch_size = tf.shape(images)[0]
        patches = tf.image.extract_patches(
            images=images,
            sizes=[1, self.patch_size, self.patch_size, 1],
            strides=[1, self.patch_size, self.patch_size, 1],
            rates=[1, 1, 1, 1],
            padding="VALID",
        )
        patch_dims = patches.shape[-1]
        patches = tf.reshape(patches, [batch_size, -1, patch_dims])
        return patches
    
    
def make_image_cuts(image_path, target_path):

    patch_size = 512  # Size of the patches to be extract from the input images
    image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(image_path))
    mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(target_path))
    # images = tf.convert_to_tensor([image,mask])
    images = tf.convert_to_tensor(np.asarray([image,mask]))
    patches = Patches(patch_size)(images)
    image_patches_list = []
    mask_patches_list = []
    for k in [0,1]:
        for i, patch in enumerate(patches[k]): # imagesv
            patch_img = tf.reshape(patch, (patch_size, patch_size, 3))
            if k:
                mask_patches_list.append(patch_img.numpy().astype("uint8"))
            else:
                image_patches_list.append(patch_img.numpy().astype("uint8"))
    assert len(image_patches_list) == len(mask_patches_list)
    return image_patches_list, mask_patches_list


def get_augmentation():
    return A.Compose([
        A.VerticalFlip(p=0.5),              
        A.RandomRotate90(p=0.5)]
    )

def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform
    
    Args:
        preprocessing_fn (callbale): data normalization function 
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose
    
    """
    _transform = [
        A.Lambda(image=preprocessing_fn),
    ]
    return A.Compose(_transform)


class SatelliteImages(tf.keras.utils.Sequence):
    """Helper to iterate over the data (as Numpy arrays)."""

    def __init__(self,  
                 input_img_paths, target_img_paths, 
                 augmentation=None, preprocessing=None):
        self.img_size = (512,512)
        self.input_img_paths = input_img_paths
        self.target_img_paths = target_img_paths
        self.augmentation = augmentation
        self.preprocessing = preprocessing
        
        self.forest = np.zeros(self.img_size + (3,), dtype="uint8") + (0,255,0)

    def __len__(self):
        return len(self.target_img_paths)

    def __getitem__(self, idx):
        """Returns tuple (input, target) correspond to batch #idx."""
        input_img_path = self.input_img_paths[idx]
        target_img_path = self.target_img_paths[idx]
        images, masks = make_image_cuts(image_path=input_img_path,
                            target_path=target_img_path)
        self.cut_size = len(images)
        # print(self.cut_size)
        x = np.zeros((self.cut_size,) +
                     self.img_size + (3,), dtype="float32")
        y = np.zeros((self.cut_size,) + self.img_size + (1,), dtype="float32")
        for num in range(self.cut_size):
            image = images[num]
            mask = masks[num]
            mask = (mask == self.forest).all(axis=2,  keepdims=True).astype(int)
            
            
            # y[num] = np.expand_dims(mask, 2)
            # apply augmentations
            if self.augmentation:
                sample = self.augmentation(image=image, mask=mask)
                image, mask = sample['image'], sample['mask']
            
            # apply preprocessing
            if self.preprocessing:
                sample = self.preprocessing(image=image, mask=mask)
                image, mask = sample['image'], sample['mask']
                
            image = tf.image.convert_image_dtype(image, tf.float32)
            
            x[num] = image
            y[num] = mask

            # Ground truth labels are 1, 2, 3. Subtract one to make them 0, 1, 2:
            # y[j] -= 1
        # print("getting: index:{} len(x):{} len(y):{} len(total):{}".format(
        #     idx, len(x), len(y), len(self.target_img_paths)))
        print("--- %s seconds ---" % (time.time() - start_time))
        return x, y


if __name__ == '__main__':
    DEEPGLOBE_DIR = "/home/kent/college/Thesis/deepglobe/"
    metadata_df = pd.read_csv(
        os.path.join(DEEPGLOBE_DIR, 'metadata.csv'))
    metadata_df = (metadata_df
                    .query('split in ["train"]')
                    )
    metadata_df['sat_image_path'] = metadata_df['sat_image_path'].apply(
        lambda img_pth: os.path.join(DEEPGLOBE_DIR, img_pth))
    metadata_df['mask_path'] = metadata_df['mask_path'].apply(
        lambda img_pth: os.path.join(DEEPGLOBE_DIR, img_pth))
    
    # Split the data
    train, test = train_test_split(
        metadata_df, test_size=0.20, shuffle=True)
    val, test = train_test_split(test, test_size=0.50, shuffle=True)

    train = train.sample(n=10, random_state=23)
    val = val.sample(n=2, random_state=23)
    
    BACKBONE = 'resnet34'
    # BACKBONE = 'efficientnetb3'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    
    # Instantiate data Sequences for each split
    train_gen = SatelliteImages(
        input_img_paths=list(train["sat_image_path"]), 
        target_img_paths=list(train["mask_path"]),
        augmentation=get_augmentation(),
        preprocessing=get_preprocessing(preprocess_input),
    )
    val_gen = SatelliteImages(
        input_img_paths=list(val["sat_image_path"]), 
        target_img_paths=list(val["mask_path"]),
        augmentation=get_augmentation(),
        preprocessing=get_preprocessing(preprocess_input),
    )

    s = val_gen[0]
    # image = s[0]
    # mask = s[1]
# import matplotlib.pyplot as plt

    # preprocess input
    # x_train = preprocess_input(train_gen)
    # x_val = preprocess_input(val_gen)
    
    # define model
    # model = sm.Unet(BACKBONE, encoder_weights='imagenet', classes=7,
    #              activation="softmax", encoder_freeze=True)
    model = sm.Unet(BACKBONE, encoder_weights='imagenet', classes=1,
                 activation="sigmoid", encoder_freeze=True)
    # model.compile('Adam', loss=bce_jaccard_loss, metrics=[iou_score])
    # model.compile('Adam', loss="sparse_categorical_crossentropy", metrics=[iou_score])
    # model.summary()

    # callbacks = [
    #     tf.keras.callbacks.ModelCheckpoint("my_unet.h5", save_best_only=True)
    # ]
    # callbacks = [
    #     tf.keras.callbacks.ModelCheckpoint("save_at_{epoch}.h5"),
    # ]
    # model.compile(
    #     optimizer="Adam",
    #     loss="binary_crossentropy",
    #     metrics=["mse"],
    # )

    # model.fit(
    #     x_train, epochs=epochs, callbacks=callbacks, validation_data=x_val,
    # )



    model.compile('Adam', loss=sm.losses.bce_jaccard_loss, metrics=[sm.metrics.iou_score])
    model.summary()
    # fit model
    # model.fit(
    #     x=train_gen,
    #     epochs=2,
    #     validation_data=val_gen,
    # )
    callbacks = [
    tf.keras.callbacks.ModelCheckpoint("my_unet.h5", save_best_only=True)
    ]
    history = model.fit(
        train_gen, 
        epochs=5, 
        callbacks=callbacks, 
        validation_data=val_gen,
    )
    
    import pickle
    with open('history.pickle', 'wb') as f:
        pickle.dump(history.history, f)
    # for i in range(10):
    #     print(val_gen[i][0].shape)
    #     print(val_gen[i][0][1,:1,:1,:])

    # test_preds = model.predict(val_gen)
    
    # train model
    # history = model.fit(
    #     train_gen, 
    #     steps_per_epoch=32, 
    #     epochs=epochs, 
    #     # callbacks=callbacks, 
    #     validation_data=val_gen,
    #     validation_steps=32
    # )
    
    # test_gen = SatelliteImages(
    #     batch_size=batch_size, 
    #     img_size=img_size, 
    #     input_img_paths=list(val["sat_image_patch"]), 
    #     target_img_paths=list(val["mask_patch"])
    # )
    # test_preds = model.predict(test_gen)