#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 15 23:24:06 2022

@author: kent
"""

import os
os.environ['CUDA_VISIBLE_DEVICES'] = '0'

import cv2
import keras
import numpy as np
import matplotlib.pyplot as plt

import cv2
import matplotlib.pyplot as plt
import os
from PIL import Image
import numpy as np
import pandas as pd
from tensorflow.keras.utils import load_img, img_to_array
import random


# helper function for data visualization
def visualize(**images):
    """PLot images in one row."""
    n = len(images)
    plt.figure(figsize=(16, 5))
    for i, (name, image) in enumerate(images.items()):
        plt.subplot(1, n, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.title(' '.join(name.split('_')).title())
        plt.imshow(image.astype("uint8"))
    plt.show()
    
    
image = cv2.imread("/home/kent/college/data_sets/camvid/0001TP_006690.png")
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

mask = cv2.imread("/home/kent/college/data_sets/camvid/0001TP_006690_mask.png", 0)
 
# extract certain classes from mask (e.g. cars)
# CLASSES = ['sky', 'building', 'pole', 'road', 'pavement', 
#                'tree', 'signsymbol', 'fence', 'car', 
#                'pedestrian', 'bicyclist', 'unlabelled']

# masks = [(mask == v) for v in CLASSES]
# mask = np.stack(masks, axis=-1).astype('float')



# # add background if mask is not binary
# if mask.shape[-1] != 1:
#     background = 1 - mask.sum(axis=-1, keepdims=True)
#     mask = np.concatenate((mask, background), axis=-1)

visualize(
    image=image, 
    cars_mask=mask
)


input_dir = "/home/kent/college/data_sets/pets/images/"
target_dir = "/home/kent/college/data_sets/pets/annotations/trimaps/"

input_img_paths = sorted(
    [os.path.join(input_dir, fname)
     for fname in os.listdir(input_dir)
     if fname.endswith(".jpg")])
target_paths = sorted(
    [os.path.join(target_dir, fname)
     for fname in os.listdir(target_dir)
     if fname.endswith(".png") and not fname.startswith(".")])


num_imgs = 1000

random.Random(1337).shuffle(input_img_paths)
random.Random(1337).shuffle(target_paths)

X = np.zeros((1000,192, 192, 3), dtype=np.float32)
y = np.zeros((1000,192, 192, 1), dtype=np.float32)

for i in range(num_imgs):
    X[i] = img_to_array(load_img(input_img_paths[i], target_size=(192, 192)))
    y[i] = img_to_array(
        load_img(target_paths[i], target_size=(192,192), color_mode="grayscale"))
    
    
image = X[2]
mask = y[2]

# Ground truth labels are 1, 2, 3. Subtract one to make them 0, 1, 2:
mask -= 1

visualize(
    image=image, 
    mask=mask
)


# create the histogram
histogram, bin_edges = np.histogram(mask, bins=10, range=(0, 9))
# configure and draw the histogram figure
plt.figure()
plt.title("Grayscale Histogram")
plt.xlabel("grayscale value")
plt.ylabel("pixel count")
# plt.xlim([0.0, 5.0])  # <- named arguments do not work here

plt.plot(bin_edges[0:-1], histogram)  # <- or here
plt.show()


unique, counts = np.unique(mask, return_counts=True)
unique, counts 