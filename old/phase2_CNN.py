#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
2nd phase

Data preparation split into Train, Validation, Test
and train CNN model

T    - V    - T
80 % - 10 % - 10%

CNN: Unet
"""

import os
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
import numpy as np
import segmentation_models as sm
import albumentations as A
import time
import matplotlib.pyplot as plt

sm.set_framework('tf.keras')
sm.framework()


def get_augmentation():
    return A.Compose([
        A.VerticalFlip(p=0.5),              
        A.RandomRotate90(p=0.5)]
    )

def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform
    
    Args:
        preprocessing_fn (callbale): data normalization function 
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose
    
    """
    _transform = [
        A.Lambda(image=preprocessing_fn),
    ]
    return A.Compose(_transform)


class SatelliteImages(tf.keras.utils.Sequence):
    """Helper to iterate over the data (as Numpy arrays)."""

    def __init__(self, batch_size, 
                 img_size, input_img_paths, target_img_paths, 
                 augmentation=None, preprocessing=None):
        self.batch_size = batch_size
        self.img_size = img_size
        self.input_img_paths = input_img_paths
        self.target_img_paths = target_img_paths
        self.augmentation = augmentation
        self.preprocessing = preprocessing
        
        self.forest = np.zeros(self.img_size + (3,), dtype="uint8") + (0,255,0)

    def __len__(self):
        return len(self.target_img_paths) // self.batch_size

    def __getitem__(self, idx):
        """Returns tuple (input, target) correspond to batch #idx."""
        i = idx * self.batch_size
        batch_input_img_paths = self.input_img_paths[i: i + self.batch_size]
        batch_target_img_paths = self.target_img_paths[i: i + self.batch_size]
        # print("batch_input_img_paths", len(batch_input_img_paths))
        # print("batch_target_img_paths", len(batch_target_img_paths))
        return_batch_length = len(batch_input_img_paths)
        x = np.zeros((return_batch_length,) +
                     self.img_size + (3,), dtype="uint8")
        y = np.zeros((return_batch_length,) + self.img_size + (1,), dtype="float32")
        for num in range(return_batch_length):
            image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
                batch_input_img_paths[num]), dtype="uint8")
            mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
                batch_target_img_paths[num]), dtype="uint8")
            mask = (mask == self.forest).all(axis=2,  keepdims=True)
            # y[num] = np.expand_dims(mask, 2)
            # apply augmentations
            if self.augmentation:
                sample = self.augmentation(image=image, mask=mask)
                image, mask = sample['image'], sample['mask']
            
            # apply preprocessing
            if self.preprocessing:
                sample = self.preprocessing(image=image, mask=mask)
                image, mask = sample['image'], sample['mask']
                
            image = tf.image.convert_image_dtype(image, tf.float32)
            
            x[num] = image
            y[num] = mask

        return x, y


if __name__ == '__main__':
    PATCHES_DIR = "/home/kent/college/Thesis/deepglobe/patches/"
    metadata_patches_df = pd.read_csv(
        os.path.join(PATCHES_DIR, 'metadata_patches.csv'))

    # Split the data
    train, test = train_test_split(
        metadata_patches_df, test_size=0.20, shuffle=True)
    val, test = train_test_split(test, test_size=0.50, shuffle=True)

    train = train.sample(n=1000, random_state=23)
    val = val.sample(n=200, random_state=23)
    
    img_size = (256, 256)
    batch_size = 32
    # BACKBONE = 'resnet34'
    BACKBONE = 'vgg16'
    # BACKBONE = 'efficientnetb3'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    
    # Instantiate data Sequences for each split
    train_gen = SatelliteImages(
        batch_size=batch_size, 
        img_size=img_size, 
        input_img_paths=list(train["sat_image_patch"]), 
        target_img_paths=list(train["mask_patch"]),
        augmentation=get_augmentation(),
        preprocessing=get_preprocessing(preprocess_input),
    )
    val_gen = SatelliteImages(
        batch_size=batch_size, 
        img_size=img_size, 
        input_img_paths=list(val["sat_image_patch"]), 
        target_img_paths=list(val["mask_patch"]),
        augmentation=get_augmentation(),
        preprocessing=get_preprocessing(preprocess_input),
    )

    
    # model = sm.Unet(BACKBONE, encoder_weights='imagenet', classes=1,
    #               activation="sigmoid", encoder_freeze=True)
    model = sm.Linknet(BACKBONE, encoder_weights='imagenet', classes=1,
                  activation="sigmoid", encoder_freeze=True)
    # define optomizer
    optim = tf.keras.optimizers.Adam(0.0001)
    
    # Segmentation models losses can be combined together by '+' and scaled by integer or float factor
    dice_loss = sm.losses.DiceLoss()
    focal_loss = sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss)
    
    # actulally total_loss can be imported directly from library, above example just show you how to manipulate with losses
    # total_loss = sm.losses.binary_focal_dice_loss # or sm.losses.categorical_focal_dice_loss 
    
    metrics = [sm.metrics.IOUScore(threshold=0.5), sm.metrics.FScore(threshold=0.5)]
    
    # compile keras model with defined optimozer, loss and metrics
    model.compile(optim, total_loss, metrics)
    # model.compile('Adam', loss=sm.losses.bce_jaccard_loss, metrics=[sm.metrics.iou_score])
    model.summary()


    callbacks = [
    tf.keras.callbacks.ModelCheckpoint("my_unet.h5", save_best_only=True)
    ]
    history = model.fit(
        train_gen, 
        epochs=5, 
        callbacks=callbacks, 
        validation_data=val_gen,
    )
    
    import pickle
    with open('history.pickle', 'wb') as f:
        pickle.dump(history.history, f)


    # Plot training & validation iou_score values
    plt.figure(figsize=(12, 5))
    # plt.subplot(121)
    plt.plot(history.history['iou_score'])
    plt.plot(history.history['val_iou_score'])
    plt.title('Model iou_score')
    plt.ylabel('iou_score')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='upper left')
    # plt.show()
    plt.savefig('Model_iou_score.png', bbox_inches='tight')
    
    # Plot training & validation loss values
    plt.figure(figsize=(12, 5))
    # plt.subplot(122)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='upper left')
    # plt.show()
    plt.savefig('Model_loss.png', bbox_inches='tight')