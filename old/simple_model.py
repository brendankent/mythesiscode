#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 19 21:42:11 2022

@author: kent
"""

import numpy as np
import tensorflow as tf
# from keras.models import Sequential
# from keras.layers import Dense, Conv2D, Flatten
# from keras.utils import to_categorical

#create model
model = tf.keras.models.Sequential()
#add model layers
model.add(tf.keras.layers.Conv2D(64, kernel_size=3, activation='relu', input_shape=(10,10,1)))
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(2, activation='softmax'))
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
class DataGenerator(tf.keras.utils.Sequence):
    def __init__(self, X, y, batch_size):
        self.X = X
        self.y = y
        self.batch_size = batch_size

    def __len__(self):
        pf = '{:20.20} {:10.10}'
        # print(pf.format("self.batch_size", str(self.batch_size)))
        # print(pf.format("len(self.X)", str(len(self.X))))
        l = int(len(self.X) / self.batch_size)
        # print(pf.format("l", str(l)))
        if l*self.batch_size < len(self.X):
            l += 1
        # print(pf.format("l", str(l)))
        return l

    def __getitem__(self, index):
        X = self.X[index*self.batch_size:(index+1)*self.batch_size]
        y = self.y[index*self.batch_size:(index+1)*self.batch_size]
        # print("returning: X:", len(X), " y:",len(y))
        print("getting:",index, len(X), len(y))
        return X, y

X = np.random.rand(97,10,10,1)
y = tf.keras.utils.to_categorical(np.random.randint(0,2,97))
x_train = DataGenerator(X,y,10)
model.fit(x_train, epochs=10)
# model.fit(DataGenerator(X,y,13), epochs=10)