#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys 
import os
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style="whitegrid")
plt.style.use('seaborn-whitegrid')   
plt.rcParams['figure.dpi'] = 90
plt.rcParams['font.family'] = ['sans-serif']
plt.rcParams.update({'font.size': 13})
plt.rcParams['figure.figsize'] = 8.3,5
    

def combine_dfs():
    model_results_df = pd.read_csv(os.path.join("/home/kent/college/Thesis/analysis",
                                                     "models_results_df.csv"))
    model_results_df = model_results_df[['name',
           'onehot_mean_iou', 'iou_0', 'iou_1', 'iou_2', 'iou_3', 'iou_4', 'iou_5']]
    model_results_df = model_results_df[['name','onehot_mean_iou', 'iou_0', 'iou_1', 'iou_2', 'iou_3', 'iou_4', 'iou_5']]
    model_results_df = model_results_df.set_index('name')
    labels = ['onehot_mean_iou', 'iou_0', 'iou_1', 'iou_2', 'iou_3', 'iou_4', 'iou_5']
    replace_dict = dict(zip(labels, ["one-hot mIoU"] + satellite_class_list[:-1]))
    model_results_df.rename(columns=replace_dict, inplace=True)
    model_results_df = model_results_df.nlargest(20, 'one-hot mIoU')
    model_results_df = model_results_df.sort_values(by=['onehot_mean_iou'], ascending=True)
    g = sns.heatmap(model_results_df, cmap="Blues", vmin=0.4, vmax=0.8, yticklabels=False)
    g.set_xticklabels(g.get_xticklabels(), rotation=45, horizontalalignment='right')
    g.set_ylabel("Models (ordered by onehot_mean_iou)")
    
    base_model_results_df = pd.read_csv(os.path.join("/home/kent/college/Thesis/analysis",
                                                     "base_model_results_df.csv"))
    base_model_results_df = base_model_results_df[['testset', 'loss', 'categorical_accuracy',
           'onehot_mean_iou', 'iou_0', 'iou_1', 'iou_2', 'iou_3', 'iou_4', 'iou_5',
           'iou_6', 'sm_iou_score']]
    base_model_results_df["model_type"] = "base_model"
    
    base_model_results_df_avg = pd.read_csv(os.path.join("/home/kent/college/Thesis/analysis",
                                                     "base_model_results_df_avg.csv"))
    base_model_results_df_avg = base_model_results_df_avg[['testset', 'loss', 'categorical_accuracy',
           'onehot_mean_iou', 'iou_0', 'iou_1', 'iou_2', 'iou_3', 'iou_4', 'iou_5',
           'iou_6', 'sm_iou_score']]
    base_model_results_df_avg["model_type"] = "base_avg_model"
    
    ensemble_model_results_df = pd.read_csv(os.path.join("/home/kent/college/Thesis/analysis",
                                          "ensemble_model_results_df.csv"))
    ensemble_model_results_df = ensemble_model_results_df[['testset', 'loss', 'categorical_accuracy',
           'onehot_mean_iou', 'iou_0', 'iou_1', 'iou_2', 'iou_3', 'iou_4', 'iou_5',
           'iou_6', 'sm_iou_score']]
    ensemble_model_results_df["model_type"] = "ensemble_model"
    
    w_ensemble_model_results_df = pd.read_csv(os.path.join("/home/kent/college/Thesis/analysis",
                                          "w_ensemble_model_results_df.csv"))
    w_ensemble_model_results_df = w_ensemble_model_results_df[['testset', 'loss', 'categorical_accuracy',
           'onehot_mean_iou', 'iou_0', 'iou_1', 'iou_2', 'iou_3', 'iou_4', 'iou_5',
           'iou_6', 'sm_iou_score']]
    w_ensemble_model_results_df["model_type"] = "w_ensemble_model"  
    
    df = pd.concat([base_model_results_df, base_model_results_df_avg, ensemble_model_results_df, w_ensemble_model_results_df])
    df.reset_index(inplace=True)
    
    sns.set(rc={'figure.figsize':(8.3,5)})
    fig, ax = plt.subplots()
    sns.displot(ax=ax, data=df, x="onehot_mean_iou", hue="model_type", kind="kde", height=5, aspect=2)
    fig.tight_layout()
    plt.show()
    
def plot_histograms(df):
    std = np.std(df["onehot_mean_iou"])
    mean = np.average(df["onehot_mean_iou"])

    fig, ax = plt.subplots()
    # plt.xlim([min(data)-5, max(data)+5])
    plt.hist(df["onehot_mean_iou"], bins=20,density=True, facecolor='g', alpha=0.75)
    plt.text(.4, 8, r'$\mu=' +str(np.round(mean,2))+ ',\ \sigma=$'+ str(np.round(std,2)))
    plt.ylabel('Count of models')
    plt.xlabel('mIoU (one-hot mean IoU)')
    # plt.title('Total number of Pixels in 803 images per label in Billions')
    plt.show()
    
    
    # import matplotlib.pyplot as plt
    sns.set(style="whitegrid")
    fig, ax = plt.subplots()
    sns.histplot(ax=ax, data=df, x="onehot_mean_iou", color="magenta", label="onehot_mean_iou", kde=True, bins=20)
    ax.text(.26, 4, r'$\mu=' +str(np.round(mean,3))+ ',\ \sigma=$'+ str(np.round(std,3)), size = 18)
    # # sns.despine(left=True)
    ax.set_xlabel("mIoU (one-hot mean IoU)", size = 14, alpha=1)
    ax.set_ylabel("Count of test sets", size = 14, alpha=1)
    # ax.set_xlim(0,1)
    # plt.legend() 
    fig.tight_layout()
    plt.show()
    
    
    df = df.sort_values('onehot_mean_iou', ascending=True)
    
    fig, ax = plt.subplots()
    sns.barplot(data=df, y="name", x= "onehot_mean_iou", color="red", label="onehot_mean_iou", alpha=0.75)
    # ax.tick_params(axis='x', rotation=90)
    # sns.despine(left=True)
    ax.set_xlabel("mIoU (one-hot mean IoU)", size = 14, alpha=1)
    ax.set_ylabel("Model name", size = 14, alpha=1)
    plt.xlim(0.3, None)
    # plt.legend() 
    plt.show()