#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 24 15:53:47 2022

@author: kent
"""

import sys 
import os
import math
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
import segmentation_models as sm
import albumentations as A
import matplotlib.pyplot as plt
import pickle
from datetime import datetime
sys.path.append("/home/kent/college/Thesis/myThesisCode/")
from Create_models_v2 import load_model, load_original_dataset, load_original_classes, plot_history_from_folder
from Create_models_v2 import SatelliteAugmentationGenerator, SatellitePatchesGenerator, get_preprocessing, reverse_one_hot
from Create_models_v2 import plot_logger_from_folder, SAVE_PATH, EPOCHS, SAMPLES, STOPPING_PATIENCE, split_dataset, get_augmentation_for_train
from Analysis_models_v1 import WeightedAverage, print_model_details
sm.set_framework('tf.keras')
sm.framework()

def simple_mnist_convnet():
    
    num_classes = 10
    input_shape = (28, 28, 1)
    
    # the data, split between train and test sets
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
    # Scale images to the [0, 1] range
    x_train = x_train.astype("float32") / 255
    x_test = x_test.astype("float32") / 255
    # Make sure images have shape (28, 28, 1)
    x_train = np.expand_dims(x_train, -1)
    x_test = np.expand_dims(x_test, -1)
    print("x_train shape:", x_train.shape)
    print(x_train.shape[0], "train samples")
    print(x_test.shape[0], "test samples")
        
    # convert class vectors to binary class matrices
    y_train = tf.keras.utils.to_categorical(y_train, num_classes)
    y_test = tf.keras.utils.to_categorical(y_test, num_classes)
        
    models = []
    
    data_augmentation = tf.keras.Sequential([
      tf.keras.layers.RandomFlip("horizontal_and_vertical"),
      tf.keras.layers.RandomRotation(0.2),
    ])
    
    for m in range(5):

        # build the model
        m = tf.keras.Sequential(
            [
                tf.keras.Input(shape=input_shape),
                data_augmentation,
                tf.keras.layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
                tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
                tf.keras.layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
                tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
                tf.keras.layers.Flatten(),
                tf.keras.layers.Dropout(0.5),
                tf.keras.layers.Dense(num_classes, activation="softmax"),
            ]
        )
        m.summary()
        
        # Train the model
        batch_size = 128
        epochs = 4
        m.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
        m.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1)

        # Evaluate the trained model
        score = m.evaluate(x_test, y_test, verbose=1)
        print("Test loss:", score[0])
        print("Test accuracy:", score[1])
        
        models.append(m)
    
    
    model_input = tf.keras.Input(shape=(28, 28, 1)) #takes a list of tensors as input, all of the same shape
    model_outputs = [x(model_input) for x in models] #collects outputs of models in a list
    ensemble_output = tf.keras.layers.Average()(model_outputs) #averaging outputs
    ensemble_model = tf.keras.Model(inputs=model_input, outputs=ensemble_output)
    ensemble_model._name = "ensemble_model"
    ensemble_model.summary()
    ensemble_model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    
    
    model_input = tf.keras.Input(shape=(28, 28, 1)) #takes a list of tensors as input, all of the same shape
    model_outputs = [x(model_input) for x in models]  #collects outputs of models in a list
    ensemble_output = WeightedAverage()(model_outputs) #averaging outputs
    w_ensemble_model = tf.keras.Model(inputs=model_input, outputs=ensemble_output)
    w_ensemble_model._name = "w_ensemble_model"
    w_ensemble_model.summary()
    w_ensemble_model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    
    for layer in w_ensemble_model.layers:
        if "weighted_average" in layer.name:
            layer.trainable = True   
        else:
            layer.trainable = False 
            
    print_model_details(w_ensemble_model)
    
    w_ensemble_model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1)
    
    tf.nn.softmax(w_ensemble_model.get_weights()[-1]).numpy()
    
    for m in models:
        print(m.name)
        score = m.evaluate(x_test, y_test, verbose=1)
        print("Test loss:", score[0])
        print("Test accuracy:", score[1])
    print(ensemble_model.name)
    score = ensemble_model.evaluate(x_test, y_test, verbose=1)
    print("Test loss:", score[0])
    print("Test accuracy:", score[1])
    print(w_ensemble_model.name)
    score = w_ensemble_model.evaluate(x_test, y_test, verbose=1)
    print("Test loss:", score[0])
    print("Test accuracy:", score[1])
    
    
    
    
    
    
    
    
